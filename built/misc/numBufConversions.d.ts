export declare function numberToBuffer(num: number): Buffer;
export declare function bufferToNumber(buffer: Buffer): number;
//# sourceMappingURL=numBufConversions.d.ts.map